#Handle API, by injecting Proc into app. Server stability should be enhanced.
require 'socket'
require 'rack'

# app = Proc.new do
#     ['200', {'Content-Type' => 'text/html'}, ["Time is #{Time.now}"]]
# end

class GarnetServer
    def initialize(app, port=5678)
        @app = app
        @port = port
        run
    end

    def run
        server = TCPServer.new @port
        puts "Listening on http://localhost:#{@port}"
        loop do
            Thread.start(server.accept) do |session|
                request = session.gets
                puts request

                method, full_path = request.split(' ')

                path, query = full_path.split('?')

                status, headers, body = @app.call({
                    'REQUEST_METHOD' => method,
                    'PATH_INFO' => path,
                    'QUERY_STRING' => query
                })

                session.print "HTTP/1.1 #{status}\r\n"

                headers.each do |key, value|
                    session.print "#{key}: #{value}\r\n"
                end

                session.print "\r\n"

                body.each do |part|
                    session.print part
                end

                session.close
            end
        end
    end
end