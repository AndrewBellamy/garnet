#!/usr/bin/env ruby

require 'optparse'
require '.\http_server'
require 'rack/lobster'

# options = {}

# OptionParser.new do |op|
#     op.on("-n", "--name [String]", "Your name") do |value|
#         options[:name] = value
#     end
# end.parse!

app = Rack::Lobster.new
port = 5679

garnet = GarnetServer.new(app, port)